import React from "react";

// reactstrap components
import {
  Button
} from "reactstrap";

// core components
import DemoNavbar from "components/Navbars/Navbar";
import SimpleFooter from "components/Footers/SimpleFooter";

const divStyle = {
  background: 'RGB(255,255,255)'
};

class Menu extends React.Component {
  
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }
  render() {
    return (
      <>
        <DemoNavbar />
        <main ref="main">
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 bg-gradient-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="offset-xl-1 col-12 col-sm-12 col-xl-10" align="center">
                  <div className="card" style={divStyle}>
                    <div className="card-title mt-3 mb-3">
                      <h2>Menu</h2>
                    </div>
                    <div className="card-body mt-3 mb-3">
                      <div className="row">
                        <div className="col-lg-5 ml-lg-5 mb-3">
                          <div className="card bg-blue-darken-3" data-toggle="tooltip" title="Consulta de prospectos">
                            <div className="card-body text-white text-center bg-secondary shadow border-0">
                              <Button
                                className="btn-neutral btn-icon"
                                color="default"
                                href="/ConsultaProspectos"
                              >
                                <span className="btn-inner--icon">
                                  <i className="fa fa-users mr-5" />
                                </span>
                                <span className="nav-link-inner--text ml-1">
                                  Listado de prospectos
                                </span>
                              </Button>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-5 ml-lg-5 mb-3">
                          <div className="card bg-blue-darken-3" data-toggle="tooltip" title="Consulta de prospectos">
                            <div className="card-body text-white text-center bg-secondary shadow border-0">
                              <Button
                                className="btn-neutral btn-icon"
                                color="default"
                                href="/RegistraProspecto"
                              >
                                <span className="btn-inner--icon">
                                  <i className="fa fa-user-plus mr-5" />
                                </span>
                                <span className="nav-link-inner--text ml-1">
                                  Registrar de prospectos
                                </span>
                              </Button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
        <SimpleFooter/>
      </>
    );
  }
}

export default Menu;
