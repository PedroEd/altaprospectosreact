import React from "react";

// reactstrap components
import {
  FormGroup,
  Input,
  Row,
  Col,
  Button
} from "reactstrap";

// core components
import DemoNavbar from "components/Navbars/Navbar.js";
import SimpleFooter from "components/Footers/SimpleFooter";
import MultipleFileInput from "components/fileuploader";
import Swal from "sweetalert2";
import axios from "axios";
import {validInput } from 'components/Regex';
const divStyle = {
  background: 'RGB(255,255,255)'
};

class Landing extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      visibilityState: "hidden",
      bPass: true,
      nombre:"",
      apellidopaterno:"",
      apellidomaterno:"",
      calle:"",
      numerocasa:"",
      colonia:"",
      codigopostal:"",
      telefono:"",
      rfc:"",
      documentos:[],
    }
  }

  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }

  GuardarDocumento(files){
    this.setState({
      documentos: files
    })
  }

  ConfirmarGuardado(){
    if (this.state.nombre !== "" && validInput.test(this.state.nombre)){
      if (this.state.apellidopaterno !== "" && validInput.test(this.state.apellidopaterno)){
        if (this.state.calle !== "" && validInput.test(this.state.calle)){
          if (this.state.numerocasa !== ""){
            if (this.state.colonia !== "" && validInput.test(this.state.colonia)){
              if (this.state.codigopostal !== ""){
                if (this.state.telefono !== ""){
                  if (this.state.rfc !== "" && validInput.test(this.state.rfc)){
                    if (this.state.documentos.length !== 0){
                      Swal.fire({
                        title: '¿Quieres realizar los cambios hechos, ya no hay vuelta atras?',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: `Guardar`,
                        denyButtonText: `No Guardar`,
                      }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                          this.GuardarProspecto()
                        } else if (result.isDenied) {
                          Swal.fire('Operacion cancelada', '', 'info')
                        }
                      })
                    }else{
                      Swal.fire({icon: 'error', title: 'Oops...', text: 'Se necesita al menos un documento'})
                    }
                  }else{
                    Swal.fire({icon: 'error', title: 'Oops...', text: 'RFC es requerido'})
                  }
                }else{
                  Swal.fire({icon: 'error', title: 'Oops...', text: 'Telefono es requerido'})
                }
              }else{
                Swal.fire({icon: 'error', title: 'Oops...', text: 'Codigo postal es requerido'})
              }
            }else{
              Swal.fire({icon: 'error', title: 'Oops...', text: 'Colonia es requerido'})
            }
          }else{
            Swal.fire({icon: 'error', title: 'Oops...', text: 'Numero casa es requerido'})
          }
        }else{
          Swal.fire({icon: 'error', title: 'Oops...', text: 'Calle es requerido'})
        }
      }else{
        Swal.fire({icon: 'error', title: 'Oops...', text: 'Apellido paterno es requerido'})
      }
    }else{
      Swal.fire({icon: 'error', title: 'Oops...', text: 'Nombre es requerido'})
    }
  }

  GuardarProspecto(){
    let dataProspecto = {
      nombre:this.state.nombre,
      apellidopaterno:this.state.apellidopaterno,
      apellidomaterno:this.state.apellidomaterno,
      calle:this.state.calle,
      numerocasa:this.state.numerocasa,
      colonia:this.state.colonia,
      codigopostal:this.state.codigopostal,
      telefono:this.state.telefono,
      rfc:this.state.rfc,
      documentos:this.state.documentos
    }
    axios.post(process.env.REACT_APP_URL_SERVICE + "api/v1/insertarprospecto", dataProspecto, {
      headers: {
        Authorization: "iQtH3x5L8kqMwVnNetIygw",
      }
    }).then((response) => {
      Swal.fire({
        icon: 'success',
        title: 'El registro se guardo exitosamente',
        showConfirmButton: false,
        timer: 2000
      })
      window.location.reload()
    }).catch((err)=>{
      if (err.response) {
        Swal.fire({icon: 'error', title: 'Oops...', text: err.response.data.msg})
      }else{
        Swal.fire({icon: 'error', title: 'Oops...', text: 'Excepcion no controlada'})
      }
    });
  }

  LimpiarDatos(){
    this.setState({
      nombre:"",
      apellidopaterno:"",
      apellidomaterno:"",
      calle:"",
      numerocasa:"",
      colonia:"",
      codigopostal:"",
      telefono:"",
      rfc:"",
      documentos:[]
    })
  }

  handleChangeNombre = (e) => {
      this.setState({nombre: e.target.value});     
  }

  handleChangeApellidoP = (e) => {
    this.setState({apellidopaterno: e.target.value});     
  }

  handleChangeApellidoM = (e) => {
    this.setState({apellidomaterno: e.target.value});     
  }

  handleChangeCalle = (e) => {
    this.setState({calle: e.target.value});     
  }

  handleChangeNumCasa = (e) => {
    if(e.target.validity.valid){
      this.setState({numerocasa: e.target.value});     
    }
  }

  handleChangeColonia = (e) => {
    this.setState({colonia: e.target.value});     
  }

  handleChangeCodigoP = (e) => {
    if(e.target.validity.valid){
      this.setState({codigopostal: e.target.value});   
    }
  }

  handleChangeTelefono = (e) => {
    if(e.target.validity.valid){
      this.setState({telefono: e.target.value});  
    }   
  }

  handleChangeRFC = (e) => {
    this.setState({rfc: e.target.value});     
  }

  render() {
    return (
      <>
        <DemoNavbar />
        <main ref="main">
          <div className="position-relative">
            <section className="section section-lg section-shaped pb-250">
              <div className="shape shape-style-1 shape-default">
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
              </div>
              <div className="container-fluid mt-3">
                <div className="row">
                  <div className="offset-xl-1 col-12 col-sm-12 col-xl-10" align="center">
                    <div className="card col-8" style={divStyle}>
                      <div className="card-title mt-3 mb-3">
                        <h2>Registra el prospecto</h2>
                      </div>
                      <div className="card-body mt-3 mb-3">
                        <div className="modal-body">
                          <Row>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="Nombre">
                                  Nombre
                                </label>
                                <Input
                                  id="Nombre"
                                  pattern="^[A-Za-z0-9]+$"
                                  placeholder="" onChange={this.handleChangeNombre}
                                  type="text"
                                  value={this.state.nombre}
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                          <Row>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="ApellidoPaterno">
                                  Apellido Paterno
                                </label>
                                <Input
                                  id="ApellidoPaterno"
                                  placeholder="" onChange={this.handleChangeApellidoP}
                                  type="text"
                                  value={this.state.apellidopaterno}
                                />
                              </FormGroup>
                            </Col>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="ApellidoMaterno">
                                  Apellido Materno
                                </label>
                                <Input
                                  id="ApellidoMaterno"
                                  placeholder="" onChange={this.handleChangeApellidoM}
                                  type="text"
                                  value={this.state.apellidomaterno}
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                          <Row>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="Calle">
                                  Calle
                                </label>
                                <Input
                                  id="Calle"
                                  placeholder="" onChange={this.handleChangeCalle}
                                  type="text"
                                  value={this.state.calle}
                                />
                              </FormGroup>
                            </Col>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="Numero Casa">
                                  Número Casa
                                </label>
                                <Input
                                  id="Numero"
                                  pattern="[0-9]*"
                                  maxLength = "5"
                                  placeholder="" onChange={this.handleChangeNumCasa}
                                  type="text"
                                  value={this.state.numerocasa}
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                          <Row>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="Colonia">
                                  Colonia
                                </label>
                                <Input
                                  id="Colonia"
                                  placeholder="" onChange={this.handleChangeColonia}
                                  type="text"
                                  value={this.state.colonia}
                                />
                              </FormGroup>
                            </Col>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="CodigoPostal">
                                  Código Postal
                                </label>
                                <Input
                                  id="CodigoPostal"
                                  pattern="[0-9]*"
                                  maxLength = "6"
                                  placeholder="" onChange={this.handleChangeCodigoP}
                                  type="text"
                                  value={this.state.codigopostal}
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                          <Row>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="Telefono">
                                  Teléfono
                                </label>
                                <Input
                                  id="Telefono"
                                  pattern="[0-9]*"
                                  maxLength = "10"
                                  placeholder="" onChange={this.handleChangeTelefono}
                                  type="text"
                                  value={this.state.telefono}
                                />
                              </FormGroup>
                            </Col>
                            <Col md="6">
                              <FormGroup>
                                <label htmlFor="RFC">
                                  RFC
                                </label>
                                <Input
                                  id="RFC"
                                  placeholder="" onChange={this.handleChangeRFC}
                                  type="text"
                                  value={this.state.rfc}
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                          <MultipleFileInput GuardarDocumento={this.GuardarDocumento.bind(this)}/>
                        </div>
                        <div className="modal-footer">
                          <Button
                            color="primary"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.ConfirmarGuardado()}
                          >
                            Guardar
                          </Button>
                          <Button
                            color="secondary"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.LimpiarDatos()}
                          >
                            Limpiar
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="separator separator-bottom separator-skew">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="none"
                  version="1.1"
                  viewBox="0 0 2560 100"
                  x="0"
                  y="0"
                >
                  <polygon
                    className="fill-white"
                    points="2560 0 2560 100 0 100"
                  />
                </svg>
              </div>
            </section>
          </div>          
        </main>
        <SimpleFooter />
      </>
    );
  }
}

export default Landing;
