import React from "react";

// reactstrap components
import { Container, Row } from "reactstrap";
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
// core components
import DemoNavbar from "components/Navbars/Navbar.js";
import ButtomRenderer from "components/BtnCellRenderer.jsx";
import SimpleFooter from "components/Footers/SimpleFooter";
import axios from "axios";
import Swal from "sweetalert2";

const divStyle = {
  background: 'RGB(255,255,255)'
};

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      columnDefs: [
        {
          headerName: 'Nombre',
          field: 'Nombre',
          minWidth: 150,
          width: 150
        },
        {
          headerName: 'Apellido Paterno',
          field: 'ApellidoPaterno',
        },
        {
          headerName: 'Apellido Materno',
          field: 'ApellidoMaterno'
        },
        {
          headerName: 'Estatus',
          field: 'Estatus'
        },
        {
          headerName: ' ',
          field: 'id',
          minWidth: 100,
          cellRenderer: 'buttomRenderer',
          cellRendererParams: {
            ActualizaGrid: this.ActualizaGrid.bind(this)
          },
          suppressMenu: true,
          sortable: false,
          width: 300
        },
        {
          headerName: 'RFC',
          field: 'RFC',
          hide:true
        }
      ],
      popupParent: document.body,
      rowData: [],
      domLayout: 'autoHeight',
      defaultColDef: {
        sortable: true,
        filter: true,
        flex: 1,
        minWidth: 150,
        resizable: true
      },
      frameworkComponents: {
        buttomRenderer: ButtomRenderer
      },
      paginationPageSize: 10,
    }
  }

  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
    this.ActualizaGrid()
  }

  ActualizaGrid(){
    axios.get(process.env.REACT_APP_URL_SERVICE + "api/v1/obtenerprospectos", {
      headers: {
        Authorization: "iQtH3x5L8kqMwVnNetIygw",
      }
    }).then((response) => {
      let DataGrid = []
      for (var i = 0; i < response.data.data.length; i++) {
        DataGrid.push({
          id: i,
          Nombre: response.data.data[i].nombre,
          ApellidoPaterno: response.data.data[i].apellidopaterno,
          ApellidoMaterno: response.data.data[i].apellidomaterno,
          Estatus: response.data.data[i].estatus,
          RFC: response.data.data[i].rfc
        })
      }
      this.setState({rowData:DataGrid})
    }).catch((err)=>{
      if (err.response) {
        Swal.fire({icon: 'error', title: 'Oops...', text: err.response.data.msg})
      }else{
        Swal.fire({icon: 'error', title: 'Oops...', text: 'Excepcion no controlada'})
      }
    });
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.autoSizeAll(false)
  };

  onPageSizeChanged = (newPageSize) => {
    var value = document.getElementById('page-size').value;
    this.gridApi.paginationSetPageSize(Number(value));
  };

  autoSizeAll = (skipHeader) => {
    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function (column) {
      if (column.colId !== "id"){
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, skipHeader);
  };

  render() {
    return (
      <>
        <>
        <DemoNavbar />
        <main ref="main">
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 bg-gradient-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="offset-xl-1 col-12 col-sm-12 col-xl-10" align="center">
                  <div className="card" style={divStyle}>
                    <div className="card-title mt-3 mb-3">
                      <h2>Listado de prospectos</h2>
                    </div>
                    <div className="card-body">
                      <Container >        
                        <Row className="justify-content-center">
                            <div className="ag-theme-alpine" id="myGrid"
                              style={{
                                height: '100%',
                                width: '100%',
                              }}
                              >
                                <AgGridReact
                                columnDefs={this.state.columnDefs}
                                defaultColDef={this.state.defaultColDef}
                                rowData={this.state.rowData}
                                enableRangeSelection={true}
                                domLayout={this.state.domLayout}
                                animateRows={true}
                                popupParent={this.state.popupParent}
                                rowSelection='single'
                                pagination={true}
                                paginationPageSize={this.state.paginationPageSize}
                                frameworkComponents={this.state.frameworkComponents}
                                onGridReady={this.onGridReady}>                        
                                </AgGridReact>
                            </div>
                        </Row>
                      </Container>
                    </div>
                  </div>
                </div>                
              </div>
            </div>
          </section>
        </main>
        <SimpleFooter/>
      </>
      </>
    );
  }
}

export default Profile;
