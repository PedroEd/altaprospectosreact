import React from 'react';
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
} from "reactstrap";

// core components
import NavbarLogin from "components/Navbars/NavbarLogin";
import SimpleFooter from "components/Footers/SimpleFooter";
import { validEmail, validPassword } from 'components/Regex';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import md5 from 'md5';
import axios from "axios";

const MySwal = withReactContent(Swal)

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      visible : false,
      AlertMsg: "",
      email: "",
      password: "",
      ClassNameFormEmail: "mb-3",
      ClassNameInputEmail:""
    }
  }

  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }

  onShowAlert =()=>{
    this.setState({visible:true},()=>{
      window.setTimeout(()=>{
        this.setState({visible:false})
      },2000)
    });
  }

  handleEmailChange= (e) => {
      this.setState({email: e.target.value});
      if (validEmail.test(e.target.value)){
        this.setState({ClassNameFormEmail: "has-success mb-3", ClassNameInputEmail: "is-valid"})
      }else{
        this.setState({ClassNameFormEmail: "has-danger mb-3", ClassNameInputEmail: "is-invalid"})
      }
  }
  handlePasswordChange = (e) => {
      this.setState({password: e.target.value});
  }

  SignIn(){
    let email = this.state.email
    let password = this.state.password

    if (email !== "" && validEmail.test(email)) {
      if (password !== "" && validPassword.test(password)) {
        let dataLogin = {
          user: email,
          password: md5(password)
        }
        axios.post(process.env.REACT_APP_URL_SERVICE + "api/v1/autheticate", dataLogin
        ).then((response) => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your work has been saved',
            showConfirmButton: false,
            timer: 1500
          })
  
          sessionStorage.setItem('token', response.data.token)
          window.location.href = "/Menu"
        }).catch((err)=>{
          if (err.response) {
            Swal.fire({icon: 'error', title: 'Oops...', text: err.response.data.msg})
          }else{
            Swal.fire({icon: 'error', title: 'Oops...', text: 'Excepcion no controlada'})
          }
        });
      }else{
        MySwal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Contraseña no válida',
        })
      }
    }else{
      MySwal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Correo no válido',
      })
    }
  }

  render() {
    return (
      <>
        <NavbarLogin />
        <main ref="main">
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 bg-gradient-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <Container className="pt-lg-7">        
              <Row className="justify-content-center">
                <Col lg="5">                
                  <Card className="bg-secondary shadow border-0">                    
                    <CardHeader className="bg-white">
                      <div className="text-muted text-center">
                        <h4>Login</h4>
                      </div>
                    </CardHeader>
                    <CardBody className="px-lg-5 py-lg-5">
                      <Form role="form">
                        <FormGroup className={this.state.ClassNameFormEmail}>
                          <InputGroup className="input-group">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input 
                              id="email" 
                              name="email"
                              className={this.state.ClassNameInputEmail}
                              placeholder="Email" 
                              type="email" 
                              autoFocus     
                              onChange={this.handleEmailChange}
                              value={this.state.email}
                            />
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup className="input-group">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              id="password"
                              placeholder="Password"
                              type="password"
                              autoComplete="off"
                              onChange={this.handlePasswordChange}
                              value={this.state.password}
                            />
                          </InputGroup>
                        </FormGroup>
                        
                        <div className="text-center">
                          <Button
                            className="my-4"
                            color="primary"
                            type="button"
                            onClick={() => this.SignIn()}
                          >
                            Sign in
                          </Button>
                        </div>
                      </Form>
                    </CardBody>
                  </Card>
                  <Row className="mt-3">
                    <Col xs="6">
                      <a
                        className="text-light"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        <small>Forgot password?</small>
                      </a>
                    </Col>
                    <Col className="text-right" xs="6">
                      <a
                        className="text-light"
                        href="/register-page"
                        onClick={e => e.preventDefault()}
                      >
                        <small>Create new account</small>
                      </a>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
          </section>
        </main>
        <SimpleFooter />
      </>
    );
  }
}

export default Login;
