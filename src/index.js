import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "assets/vendor/nucleo/css/nucleo.css";
import "assets/vendor/font-awesome/css/font-awesome.min.css";
import "assets/scss/argon-design-system-react.scss?v1.1.0";

import RegistarProspecto from "views/RegistrarProspecto";
import Login from "views/Login.js";
import ConsultaProspectos from "views/ConsultaProspectos";
import Menu from "views/Menu.js";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" exact render={props => <Login {...props} />} />
      <Route path="/Menu" exact render={props => <Menu {...props} />} />
      <Route path="/ConsultaProspectos" exact render={props => <ConsultaProspectos {...props} />} />
      <Route path="/RegistraProspecto" exact render={props => <RegistarProspecto {...props} />} />
      <Redirect to="/" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
