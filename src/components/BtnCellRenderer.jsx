import React, { Component } from 'react';
import { Modal, Button, Row, Col, FormGroup, Input} from "reactstrap";
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import axios from "axios";

const MySwal = withReactContent(Swal);

export default class ButtomRenderer extends Component {
  constructor(props) {
    super(props);

    this.SendFunction = this.SendFunction.bind(this);
    this.state = {
      cellValue: ButtomRenderer.getValueToDisplay(props),
      ModalConsulta: false,
      ModalEvaluacion: false,
      dataProspecto: {
        nombre:"",
        apellidopaterno:"",
        apellidomaterno:"",
        calle:"",
        numerocasa:"",
        colonia:"",
        codigopostal:"",
        telefono:"",
        rfc:"",
        estatus:"",
        observaciones:"",
        documentos:[]
      },
      observaciones:"",
      estatus:"",
      visibilityState: "hidden"
    };
  }

  // update cellValue when the cell's props are updated
  /*static getDerivedStateFromProps(nextProps) {
    return {
      cellValue: ButtomRenderer.getValueToDisplay(nextProps),
    };
  }*/

  SendFunction (){
    this.props.ActualizaGrid()
  }

  toggleModal = state => {
    this.setState({
      [state]: !this.state[state]
    });
  };

  consultaProspecto (tipo) {
    if (tipo === 1){
      axios.get(process.env.REACT_APP_URL_SERVICE + "api/v1/obtenerprospecto", {
        headers: {
          Authorization: "iQtH3x5L8kqMwVnNetIygw",
        },
        params: {
          rfc: this.props.data.RFC
        }
      }).then((response) => {
        this.setState({
          dataProspecto:{
            nombre: this.props.data.Nombre,
            apellidopaterno: this.props.data.ApellidoPaterno,
            apellidomaterno: this.props.data.ApellidoMaterno,
            calle: response.data.data[0].calle,
            numerocasa: response.data.data[0].numerocasa,
            colonia: response.data.data[0].colonia,
            codigopostal: response.data.data[0].codigopostal,
            telefono: response.data.data[0].telefono,
            rfc: this.props.data.RFC,
            documentos: response.data.data[0].documentos,
            estatus: response.data.data[0].estatus,
            observaciones: response.data.data[0].observaciones
          },
          estatus:response.data.data[0].estatus
        })
      }).catch((err)=>{
        if (err.response) {
          Swal.fire({icon: 'error', title: 'Oops...', text: err.response.data.msg})
        }else{
          Swal.fire({icon: 'error', title: 'Oops...', text: 'Excepcion no controlada'})
        }
      });

      if (this.state.dataProspecto.estatus === "Rechazado"){
        this.setState({
          visibilityState: "visible"
        })
      }
    }else if(tipo===2){
      if(this.state.dataProspecto.estatus === "Rechazado" || this.state.dataProspecto.estatus === "Autorizado"){
        this.toggleModal("ModalEvaluacion")
        MySwal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Solo puede evaluar prospectos con estatus "Enviado"',
        })
      }
    }
  }

  ConfirmarGuardado(){
    Swal.fire({
      title: '¿Quieres realizar los cambios hechos?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Guardar`,
      denyButtonText: `No Guardar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        if (this.state.estatus !== "Enviado"){
          this.ActualizarProspecto()
        }else{
          Swal.fire('No se puede actualizar le prospecto con el mismo estatus', '', 'info')
        }
      } else if (result.isDenied) {
        Swal.fire('Operacion cancelada', '', 'info')
      }
    })
  }

  ActualizarProspecto(){
    let updateJson = {
      rfc: this.state.dataProspecto.rfc,
      estatus: this.state.estatus,
      observaciones: this.state.observaciones
    }
    axios.put(process.env.REACT_APP_URL_SERVICE + "api/v1/actualizaprospecto", updateJson, {
      headers: {
        Authorization: "iQtH3x5L8kqMwVnNetIygw",
      }
    }).then((response) => {
      this.SendFunction()
      this.toggleModal("ModalEvaluacion")
      Swal.fire({
        icon: 'success',
        title: 'Se actualizo de manera exitosa',
        showConfirmButton: false,
        timer: 2000
      })
    }).catch((err)=>{
      if (err.response) {
        Swal.fire({icon: 'error', title: 'Oops...', text: err.response.data.msg})
      }else{
        Swal.fire({icon: 'error', title: 'Oops...', text: 'Excepcion no controlada'})
      }
    });
  }

  DescargarArchivo(i){
    let archivobase64 = this.state.dataProspecto.documentos[i].fileData
    window.location.href = 'data:application/octet-stream;base64,' + archivobase64;
  }

  handleEstatusChange = (e) => {
    this.setState({estatus: e.target.value});
    if (e.target.value === "Rechazado"){
      this.setState({
        visibilityState: "visible"
      })
    }else{
      this.setState({
        visibilityState: "hidden"
      })
    }
  }

  handleObservacionesChange = (e) => {
    this.setState({observaciones: e.target.value});
  }

  render() {
    return (
      <span>

        <Button className="btn-icon btn-3" color="secondary" onClick={() => this.toggleModal("ModalConsulta")} size="sm">
          <span className="btn-inner--icon">
            <i className="ni ni-zoom-split-in" />
          </span>
          <span className="btn-inner--text">Consultar</span>
        </Button>
        <Button className="btn-icon btn-3" color="secondary" onClick={() => this.toggleModal("ModalEvaluacion")} size="sm">
          <span className="btn-inner--icon">
            <i className="fa fa-user" />
          </span>
          <span className="btn-inner--text">Evaluar</span>
        </Button>


        <Modal
            className="modal-dialog-centered"
            isOpen={this.state.ModalConsulta}
            onEnter={() => this.consultaProspecto(1)}
            onOpened={() => this.consultaProspecto(1)}
            toggle={() => this.toggleModal("ModalConsulta")}
            >
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Consulta de prospecto {this.state.estatus}
                </h5>
                <button
                  aria-label="Close"
                  className="close"
                  data-dismiss="modal"
                  type="button"
                  onClick={() => this.toggleModal("ModalConsulta")}
                >
                  <span aria-hidden={true}>×</span>
                </button>
              </div>
              <div className="modal-body">
                <Row>
                  <Col md="8">
                    <FormGroup>
                      <label htmlFor="Nombre">
                        Nombre
                      </label>
                      <Input
                        readOnly
                        id="Nombre"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.nombre}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="ApellidoPaterno">
                        Apellido Paterno
                      </label>
                      <Input
                        readOnly
                        id="ApellidoPaterno"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.apellidopaterno}
                      />
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="ApellidoMaterno">
                        Apellido Materno
                      </label>
                      <Input
                        readOnly
                        id="ApellidoMaterno"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.apellidomaterno}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="Calle">
                        Calle
                      </label>
                      <Input
                        readOnly
                        id="Calle"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.calle}
                      />
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="Numero">
                        Número Casa
                      </label>
                      <Input
                        readOnly
                        id="Numero"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.numerocasa}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="Colonia">
                        Colonia
                      </label>
                      <Input
                        readOnly
                        id="Colonia"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.colonia}
                      />
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="CodigoPostal">
                        Código Postal
                      </label>
                      <Input
                        readOnly
                        id="CodigoPostal"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.codigopostal}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="Telefono">
                        Teléfono
                      </label>
                      <Input
                        readOnly
                        id="Telefono"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.telefono}
                      />
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="RFC">
                        RFC
                      </label>
                      <Input
                        readOnly
                        id="RFC"
                        placeholder=""
                        type="text"
                        value={this.state.dataProspecto.rfc}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="Estatus">
                        Estatus
                      </label>
                      <Input readOnly type="select" id="Estatus" value={this.state.dataProspecto.estatus} >
                        <option>Enviado</option>
                        <option>Autorizado</option>
                        <option>Rechazado</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <div><label>Documentos</label></div>
                {
                  this.state.dataProspecto.documentos.map((x, i) => {
                    return (
                      <Row style={{ marginTop: 5 }} key={i}>
                        <Col md="12">
                          <a download={x.fileNom} href={'data:application/octet-stream;base64,'+x.fileData}>{x.fileNom}</a>
                        </Col>
                      </Row>
                    )
                  })
                }
                <Row style={{visibility: this.state.visibilityState, marginTop: 10 }}>
                  <Col md="12">
                    <FormGroup>
                      <label htmlFor="Observaciones">
                        Observaciones
                      </label>
                      <Input
                        readOnly
                        id="Observaciones"
                        placeholder=""
                        type="textarea"
                        value={this.state.dataProspecto.observaciones}
                        onChange={this.handleObservacionesChange}
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </div>
              <div className="modal-footer">
                <Button
                  color="secondary"
                  data-dismiss="modal"
                  type="button"
                  onClick={() => this.toggleModal("ModalConsulta")}
                >
                  Cerrar
                </Button>
                
              </div>
            </Modal>
        <Modal
        className="modal-dialog-centered"
        isOpen={this.state.ModalEvaluacion}
        onOpened={() => this.consultaProspecto(2)}
        onEnter={() => this.consultaProspecto(2)}
        toggle={() => this.toggleModal("ModalEvaluacion")}
        >
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Evaluacion de prospecto
            </h5>
            <button
              aria-label="Close"
              className="close"
              data-dismiss="modal"
              type="button"
              onClick={() => this.toggleModal("ModalEvaluacion")}
            >
              <span aria-hidden={true}>×</span>
            </button>
          </div>
          <div className="modal-body">
            <Row>
              <Col md="8">
                <FormGroup>
                  <label htmlFor="Nombre">
                    Nombre
                  </label>
                  <Input
                    readOnly
                    id="Nombre"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.nombre}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="ApellidoPaterno">
                    Apellido Paterno
                  </label>
                  <Input
                    readOnly
                    id="ApellidoPaterno"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.apellidopaterno}
                  />
                </FormGroup>
              </Col>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="ApellidoMaterno">
                    Apellido Materno
                  </label>
                  <Input
                    readOnly
                    id="ApellidoMaterno"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.apellidomaterno}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="Calle">
                    Calle
                  </label>
                  <Input
                    readOnly
                    id="Calle"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.calle}
                  />
                </FormGroup>
              </Col>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="Numero">
                    Número Casa
                  </label>
                  <Input
                    readOnly
                    id="Numero"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.numerocasa}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="Colonia">
                    Colonia
                  </label>
                  <Input
                    readOnly
                    id="Colonia"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.colonia}
                  />
                </FormGroup>
              </Col>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="CodigoPostal">
                    Código Postal
                  </label>
                  <Input
                    readOnly
                    id="CodigoPostal"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.codigopostal}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="Telefono">
                    Teléfono
                  </label>
                  <Input
                    readOnly
                    id="Telefono"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.telefono}
                  />
                </FormGroup>
              </Col>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="RFC">
                    RFC
                  </label>
                  <Input
                    readOnly
                    id="RFC"
                    placeholder=""
                    type="text"
                    value={this.state.dataProspecto.rfc}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <FormGroup>
                  <label htmlFor="Estatus">
                    Estatus
                  </label>
                  <Input type="select" id="Estatus" value={this.state.estatus} onChange={this.handleEstatusChange} >
                    <option>Enviado</option>
                    <option>Autorizado</option>
                    <option>Rechazado</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row style={{visibility: this.state.visibilityState}}>
              <Col md="12">
                <FormGroup>
                  <label htmlFor="Observaciones">
                    Observaciones
                  </label>
                  <Input
                    id="Observaciones"
                    placeholder=""
                    type="textarea"
                    value={this.state.observaciones}
                    onChange={this.handleObservacionesChange}
                  />
                </FormGroup>
              </Col>
            </Row>
          </div>
          <div className="modal-footer">
          <Button
              color="primary"
              data-dismiss="modal"
              type="button"
              onClick={() => this.ConfirmarGuardado()}
            >
              Guardar
            </Button>
            <Button
              color="secondary"
              data-dismiss="modal"
              type="button"
              onClick={() => this.toggleModal("ModalEvaluacion")}
            >
              Cerrar
            </Button>
          </div>
        </Modal>
      </span>
    );
  }

  static getValueToDisplay(params) {
    return params.valueFormatted ? params.valueFormatted : params.value;
  }
}
