import React from "react";
// JavaScript plugin that hides or shows a component based on your scroll
import Headroom from "headroom.js";
// reactstrap components
import {
  Button,
  UncontrolledCollapse,
  NavbarBrand,
  Navbar,
  NavItem,
  Nav,
  Container
} from "reactstrap";

class DemoNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      visibilityState: "visible"
    }
  }
  componentDidMount() {
    if (sessionStorage.getItem('token') === null){
      window.location.href = "/"
    }
    let headroom = new Headroom(document.getElementById("navbar-main"));
    // initialise
    headroom.init();
    if (window.location.pathname === "/Menu"){
      this.setState({
        visibilityState: "hidden"     
      })
    }
  }

  CerrarSesion(){
    sessionStorage.clear()
    window.location.href = "/"
  }

  state = {
    collapseClasses: "",
    collapseOpen: false
  };

  onExiting = () => {
    this.setState({
      collapseClasses: "collapsing-out"
    });
  };

  onExited = () => {
    this.setState({
      collapseClasses: ""
    });
  };

  returnAction(){
    window.location.href = "/Menu"
  }

  render() {
    return (
      <>
        <header className="header-global">
          <Navbar
            className="navbar-main navbar-transparent navbar-light headroom"
            expand="lg"
            id="navbar-main"
          >
            <Container>
              <NavbarBrand className="mr-lg-5" >
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  onClick={() => this.returnAction()}
                  style={{visibility: this.state.visibilityState}}
                >
                  <span className="btn-inner--icon">
                    <i className="fa fa-reply mr-2" />
                  </span>
                  <span className="nav-link-inner--text ml-1">
                    Regresar
                  </span>
                </Button>
              </NavbarBrand>
              <button className="navbar-toggler" id="navbar_global">
                <span className="navbar-toggler-icon" />
              </button>
              <UncontrolledCollapse
                toggler="#navbar_global"
                navbar
                className={this.state.collapseClasses}
                onExiting={this.onExiting}
                onExited={this.onExited}
              >
                <Nav className="align-items-lg-center ml-lg-auto" navbar>
                  <NavItem className="d-none d-lg-block ml-lg-4">
                    <Button
                      className="btn-neutral btn-icon"
                      color="default"
                      onClick={() => this.CerrarSesion()}
                    >
                      <span className="btn-inner--icon">
                        <i className="ni ni-user-run mr-2" />
                      </span>
                      <span className="nav-link-inner--text ml-1">
                        Cerrar sesión
                      </span>
                    </Button>
                  </NavItem>
                </Nav>
              </UncontrolledCollapse>
            </Container>
          </Navbar>
        </header>
      </>
    );
  }
}

export default DemoNavbar;
