import React, { useState } from 'react';
import { Button } from "reactstrap";

const MultipleFileInput = (props)  => {

  const [files, setFiles] = useState([]);
  const [inputList, setInputList] = useState([{ FileNom: "", FileData: "", fileBase64: "", fileType: "" }]);

  const handleInputChange = (e, index) => {
      const { name, value } = e.target;
      const list = [...inputList];
      if (name === "FileData"){
        let file = e.target.files[0];

        if (file !== undefined && file !== null){
          let type = file.type.replace("application/", "");
          type = type.replace("image/", "");
          let file_reader = new FileReader();
          file_reader.onload = () => {
            setFiles([...files, { file_id: index, uploaded_file: file_reader.result }]);
            list[index]["fileType"] = type
            list[index]["fileBase64"] = file_reader.result
          };
          file_reader.readAsDataURL(file);
        }
      }
      list[index][name] = value;
      setInputList(list);
      props.GuardarDocumento(inputList)
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
      const list = [...inputList];
      list.splice(index, 1);
      setInputList(list);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
      setInputList([...inputList, { FileNom: "", FileData: "" }]);
  };

  return (
    <form>
      <div><label></label></div>
      <div><label>Documentos</label></div>
        {inputList.map((x, i) => {
            return (
                <div style={{ marginTop: 5 }} key={i}>
                  <label htmlFor={""}>
                    Nombre documento
                  </label>
                  <input
                    //onChange={onFileUpload}
                    name="FileNom"
                    onChange={e => handleInputChange(e, i)}
                    value={x.FileNom}
                    id={"FileNom" + inputList.length}
                    accept=".jpeg, .pdf"
                    type="text"
                  />
                  <input
                  //onChange={onFileUpload}
                  className="ml10"
                  name="FileData"
                  onChange={e => handleInputChange(e, i)}
                  id={"File " + inputList.length}
                  accept=".jpeg, .pdf"
                  type="file"
                  />
                  {inputList.length !== 1 && 
                  <Button 
                  className="btn-neutral btn-icon mr10"
                  color="default"
                  onClick={() => handleRemoveClick(i)}>
                      <span className="btn-inner--icon">
                          <i className="fa fa-minus" />
                      </span>
                  </Button>}
                  {inputList.length - 1 === i && 
                  <Button 
                  className="btn-neutral btn-icon mr10"
                  color="default"
                  onClick={handleAddClick}>
                      <span className="btn-inner--icon">
                          <i className="fa fa-plus" />
                      </span>
                  </Button>}
              </div>
            )
        })}
        {//<div style={{ marginTop: 20 }}>{JSON.stringify(inputList)}</div>
        }
      
    </form>
  );
};

export default MultipleFileInput;